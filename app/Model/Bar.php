<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Bar extends Model
{
    public function bebidas(){
        return $this->hasMany(Bebida::class);
    }
}
