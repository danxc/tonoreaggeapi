<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Bebida extends Model
{
    public function bar(){
        return $this->belongsTo(Bar::class);
    }
}
