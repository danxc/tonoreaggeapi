<?php

namespace App\Http\Resources\Bar;

use App\Model\Bar;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BarCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'bar'=>$this->nome,
            'bairro'=>$this->bairro,
            'href'=>[
                'bar'=>route('bar.show', Bar::findOrFail($this->id))
            ],
        ];
    }
}
