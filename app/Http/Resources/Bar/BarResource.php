<?php

namespace App\Http\Resources\Bar;

use App\Model\Bar;
use Illuminate\Http\Resources\Json\JsonResource;

class BarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'nome'=> $this->nome,
            'bairro'=>$this->bairro,
            'data_de_atualizacao'=>$this->updated_at,
            'href'=>[
                'bebidas'=>route('bebida.index', $this)
            ],
        ];
    }
}
