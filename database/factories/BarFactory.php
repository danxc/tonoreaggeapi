<?php

use Faker\Generator as Faker;

/* @var \Illuminate\Database\Eloquent\Factory $factory*/

$factory->define(App\Model\Bar::class, function (Faker $faker) {
    return [
        'nome'=>$faker->company,
        'bairro'=>$faker->city
    ];
});
