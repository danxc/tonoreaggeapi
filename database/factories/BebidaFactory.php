<?php

use Faker\Generator as Faker;
/* @var \Illuminate\Database\Eloquent\Factory $factory*/

$factory->define(App\Model\Bebida::class, function (Faker $faker) {
    return [
        'nome'=> $faker->word,
        'alcoolica'=> $faker->boolean,
        'preco'=> $faker->randomFloat('2',1,10),
        'bar_id'=> function(){
            return \App\Model\Bar::all()->random();
        }


    ];
});
