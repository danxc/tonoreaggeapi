<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Model\Bar::class,50)->create();
        factory(App\Model\Bebida::class, 250)->create();
    }
}
